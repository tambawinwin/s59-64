import { Row, Col, Button, Card, Form } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import { useEffect, useState, useContext } from 'react';
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const [product, setProduct] = useState(null);
  const [quantity, setQuantity] = useState(1);
  const { id } = useParams();
  const navigate = useNavigate();
  const { user } = useContext(UserContext);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then((response) => response.json())
      .then((data) => setProduct(data))
      .catch((error) => console.log(error));
  }, [id]);

  const handleOrder = () => {
    // Check if the user is logged in
    if (user.id) {
      // Create an object with the product id and quantity
      const orderData = {
        productId: product._id,
        quantity: quantity,
        productName: product.productName
      };
      // Send a post request to the backend order route with the order data and the user token
      fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify(orderData),
      })
        .then((response) => response.json())
        .then((data) => {
          // If the order is successful, show a success message and navigate to the user's order page
          if (data._id) {
            Swal2.fire({
              title: "Order successful!",
              icon: "success",
              text: "You have successfully ordered this product.",
            });
            navigate("/");
          } else {
            // If the order is not successful, show an error message
            Swal2.fire({
              title: "Order failed!",
              icon: "error",
              text: data,
            });
          }
        })
        .catch((error) => console.log(error));
    } else {
      // If the user is not logged in, show a warning message and ask them to log in or register first
      Swal2.fire({
        title: "Please log in or register first",
        icon: "warning",
        text: "You need to have an account to order products.",
      });
      navigate("/register")
    }
  };



  if (!product) {
    return <div>Loading...</div>;
  }

  return (
    <Row className= 'mt-3 mb-3' >
      <Col>
        <Card className="mx-auto" style={{width: '18rem'}} >
          <Card.Img variant="top" src={product.image} alt={product.productName} />
          <Card.Body>
            <Card.Title>{product.productName}</Card.Title>
            <Card.Text>{product.productDescription}</Card.Text>
            <Card.Text>Price: <span>&#8369;</span> {product.productPrice}</Card.Text>
            <Form>
              <Form.Group controlId="quantity">
                <Form.Label>Quantity</Form.Label>
                <div className="input-group">
                  <Button
                    variant="outline-secondary"
                    id="btn-minus"
                    onClick={() => setQuantity(quantity - 1)}
                    disabled={quantity === 1}
                  >
                    -
                  </Button>
                  <Form.Control
                    type="number"
                    min={1}
                    value={quantity}
                    onChange={(e) => setQuantity(parseInt(e.target.value))}
                    className="text-center"
                  />
                  <Button
                    variant="outline-secondary"
                    id="btn-plus"
                    onClick={() => setQuantity(quantity + 1)}
                  >
                    +
                  </Button>
                </div>
              </Form.Group>
            </Form>
            <Card.Text></Card.Text>
            <Button variant="dark" onClick={handleOrder}>
              Order
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
