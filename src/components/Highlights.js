import Carousel from 'react-bootstrap/Carousel';

export default function Highlights() {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://sageisland.com/wp-content/uploads/2018/04/warehouse-skateboards-ecommerce-digital-banner-ads.jpg"
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://daklinic.com/cdn/shop/files/CompleteSaleBanner_2000x.jpg?v=1652410838"
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://www.tennessean.com/gcdn/presto/2022/10/04/PNAS/39752aa3-c3ef-4b96-b2d8-aba572f51f64-skateshop003.JPG"
          alt="Third slide"
        />
      </Carousel.Item>
    </Carousel>
  );
}

