import { Button, Row, Col } from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function Banner(){
	return(
		<Row>
            <Col className="p-5">
                <h1>Sugbo Skate Warehouse</h1>
                <p>Cebu's One Stop Skate Shop! Naa na diri tanan wala nakay lain adtuan!</p>
                <Button as = {Link} to = '/products' variant="dark">Shop now!</Button>
            </Col>
        </Row>

	)
}