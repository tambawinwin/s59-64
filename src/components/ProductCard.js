import { Card, Button, Col, Row } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({ productProp }) {
  const { _id, productName, productDescription, productPrice, image } = productProp;

  return (
    <Row className= 'mt-3 mb-3' >
      <Col className='d-flex'>
        <Card className="mx-auto" style={{width: '20rem'}} >
          <Card.Img variant="top" src={image} alt={productName} />
          <Card.Body>
            <Card.Title>{productName}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{productDescription}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>{`₱ ${productPrice}`}</Card.Text>
            <Button as={Link} to={`/products/${_id}`} variant="dark">
              Details
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

