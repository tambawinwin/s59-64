import { Fragment, useState, useEffect } from 'react';
import {Row, Col} from 'react-bootstrap';
import ProductCard from '../components/ProductCard';

export default function Products(){
	

	const [products, setProducts] = useState([]);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(null);

	// we are goin to add an useEffect here so that in every time that we refresh our application it will fetch the updated content of our courses.

	useEffect(() => {
		const fetchProducts = async () => {
			try {
				const response = await fetch(`${process.env.REACT_APP_API_URL}/products/active`);
				const data = await response.json();
				setProducts(data.map(product => {
					return(
						<ProductCard key = {product._id} productProp = {product} />
						)
				}));
				setLoading(false);
			} catch (err) {
				setError(err.message);
				setLoading(false);
			}
		};
		fetchProducts();
	}, []);


	return (
		<Fragment>
			{loading && <p>Loading...</p>}
			{error && <p>{error}</p>}
			<Row>
				{products.map((product) => (
					<Col key={product._id} sm={12} md={6} lg={4} xl={3} >
						{product}
					</Col>
				))}
			</Row>
		</Fragment>
	)
}
