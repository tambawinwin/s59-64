import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col} from 'react-bootstrap';
import {Navigate, useNavigate, Link} from 'react-router-dom';

// sweetalert2 is a simple and useful package for generating user alert with ReactJS
import Swal2 from 'sweetalert2';

import UserContext from '../UserContext';

export default function Login() {

    const navigate = useNavigate();

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);


    //Allows us to consume the UserContext object and it's properties to use for user validation
    const { user, setUser } = useContext(UserContext);

    // const [user, setUser] = useState(localStorage.getItem('email'));

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

       
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
              'Content-type': 'application/json',
            },
            body: JSON.stringify({
              email: email,
              password: password,
            }),
          })
            .then((response) => {
              if (!response.ok) {
                throw new Error('Login unsuccessful');
              }
              return response.json();
            })
            .then((data) => {
              localStorage.setItem('token', data.accessToken);
              retrieveUserDetails(data.accessToken);

              Swal2.fire({
                title: 'Login successful!',
                icon: 'success',
                text: 'Welcome You Rock!',
              });

              navigate('/');
            })
            .catch((error) => {
              Swal2.fire({
                title: 'Login unsuccessful!',
                icon: 'error',
                text: 'Check your login credentials and try again!',
              });
            });
        }

    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })

    }


    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== '' && email.length<=20){
            setIsActive(false);
        }else{
            setIsActive(true);
        }

    }, [email, password]);

    return (
        user.id === null || user.id === undefined
        ?
            <Row>
                <Col className = 'col-6 mx-auto'>
                    <h1 className = 'text-center mt-2'>Login</h1>
                    <Form onSubmit={(e) => authenticate(e)}>
                        <Form.Group controlId="userEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>
                        
                        <Button variant="dark" type="submit" id="submitBtn" disabled = {isActive} className ='mt-3'>
                                Login
                        </Button>
                    </Form>
                    <p className="text-center mt-3">
                        Don't have an account yet?{' '}
                        <Link to="/register">Click here</Link> to register.
                    </p>
                </Col>
            </Row>

        :
          <Navigate to = '/*' />
    )
}