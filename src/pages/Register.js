// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import { Button, Form, Row, Col} from 'react-bootstrap';
// we need to import the useState from the react
import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate, Link} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal2 from 'sweetalert2';

export default function Register(){

	const navigate = useNavigate();
	const {user, setUser} = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isPassed, setIsPassed] = useState(true);

	const [isDisabled, setIsDisabled] = useState(true);

	// we are going to add/create a state that will declare wether the password1 and password2 is equal
	const [isPasswordMatch, setIsPasswordMatch] = useState(true);

	// when the email changes it will have a side effect that will console its value
	useEffect(() =>{
		if(email.length > 20){
			setIsPassed(false)
		}else{
			setIsPassed(true)
		}
	}, [email]);

	// this useEffect will disable or enable our sign up button
	useEffect(()=> {
		// we are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button
		if(email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && email.length <= 20){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}
	}, [email, password1, password2])

	// function to simulate user registration
	function registerUser(event) {
		// prevent page reloading
		event.preventDefault();

		/*alert('Thank you for registering!');
		
		navigate('/login');

		setEmail('');
		setPassword1('');
		setPassword2('');
	}*/

	fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
	     method: 'POST',
	     headers: {
	       'Content-Type': 'application/json',
	     },
	     body: JSON.stringify({
	       firstName: firstName,
	       lastName: lastName,
	       email: email,
	       mobileNo: mobileNumber,
	       password: password1,
	     }),
	   })
	     .then((response) => {
	       if (!response.ok) {
	         throw new Error('Registration unsuccessful');
	       }
	       return response.json();
	     })
	     .then((data) => {
	       if (data.emailExists) {
	         Swal2.fire({
	           title: 'Registration unsuccessful!',
	           icon: 'error',
	           text: 'Email already exists.',
	         });
	       } else {
	         Swal2.fire({
	           title: 'Thank you for registering!',
	           icon: 'success',
	           text: 'You may now login!',
	         });
	         navigate('/login');
	       }
	     })
	     .catch((error) => {
	       Swal2.fire({
	         title: 'Registration unsuccessful!',
	         icon: 'error',
	         text: 'Please try again!',
	       });
	     });
	 }

	// useEffect to validate wether the password1 is equal to password2
	useEffect(() => {
		if(password1 !== password2){
			setIsPasswordMatch(false);
		}else{
			setIsPasswordMatch(true);
		}

	}, [password1, password2])

	return(
		user.id === null || user.id === undefined
		?
			<Row>
				<Col className = "col-6 mx-auto">
					<h1 className = "text-center">Register</h1>
					<Form onSubmit = {event => registerUser(event)}>
					  <Form.Group className="mb-3" controlId="formBasicFirstName">
					    <Form.Label>First Name</Form.Label>
					    <Form.Control 
					    	type="name" 
					    	placeholder="Enter first name"
					    	value = {firstName}
					    	onChange = {event => setFirstName(event.target.value)} 
					    	/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicLastName">
					    <Form.Label>Last Name</Form.Label>
					    <Form.Control 
					    	type="name" 
					    	placeholder="Enter last name"
					    	value = {lastName}
					    	onChange = {event => setLastName(event.target.value)} 
					    	/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicEmail">
					    <Form.Label>Email address</Form.Label>
					    <Form.Control 
					    	type="email" 
					    	placeholder="Enter email"
					    	value = {email}
					    	onChange = {event => setEmail(event.target.value)} 
					    	/>
					    <Form.Text className="text-danger" hidden = {isPassed}>
					      The email should not exceed 15 characters!
					    </Form.Text>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicMobileNumber">
					    <Form.Label>Mobile Number</Form.Label>
					    <Form.Control 
					    	type="contact" 
					    	placeholder="Enter Mobile Number"
					    	value = {mobileNumber}
					    	onChange = {event => setMobileNumber(event.target.value)} 
					    	/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicPassword1">
					    <Form.Label>Password</Form.Label>
					    <Form.Control 
					    	type="password" 
					    	placeholder="Password" 
					    	value = {password1}
					    	onChange = {event => setPassword1(event.target.value)}
					    	/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicPassword2">
					    <Form.Label>Confirm Password</Form.Label>
					    <Form.Control 
					    	type="password" 
					    	placeholder="Retype your nominated password"
					    	value = {password2}
					    	onChange = {event => setPassword2(event.target.value)} 
					    	/>
					    <Form.Text className="text-danger" hidden = {isPasswordMatch}>
					      The passwords does not match!
					    </Form.Text>
					  </Form.Group>

					 
					  <Button variant="dark" type="submit" disabled = {isDisabled}>
					    Sign up
					  </Button>
					</Form>
					<p className="text-center mt-3">
					   Already have an account?{' '}
					   <Link to="/login">Click here</Link> to login.
					</p>
				</Col>
			</Row>
		:
		<Navigate to = '/*' />
	)
}